#!/usr/bin/env python
# coding=utf-8


import urllib
import re
import os
import errno
import datetime
import logging
import sys
import threading

def mkdir_p(path):
  try:
    os.makedirs(path)
  except OSError as exc: # Python >2.5
    if exc.errno == errno.EEXIST:
      pass
    else: raise  

class FotologPictureDownloader (threading.Thread):
  def __init__ (self, url, path):
    logging.basicConfig(filename='fotolog.thread.log', level=logging.DEBUG)
    self.url = url
    self.path = path

  def run (self):
    urllib.urlretrieve(self.url, self.path + '.jpg')
    #logging.info('Entry saved. Post: '+title +' from date '+day)
    logging.info('Entry saved. Post: '+self.path)

class fotolog:
  def __init__(self, nombre):
    self.entrada = 0 #easy fix to date not matching with any pattern
    self.name = nombre
    self.web = 'http://www.fotolog.com/' + self.name
    self.html_head = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/></head><body>'
    self.html_footer = '</body></html>'
    print 'El fotolog escogido es ' + self.web
  
  def init_patrones(self):
    self.ptrn_title = re.compile(r'<h1>(.+)</h1>') #still works, gives description
    self.ptrn_date = re.compile(r'<br class="clear"><br class="clear"><br class="clear">(.+?)<span class="flog_block_views float_right">', re.DOTALL)
    self.ptrn_date_format = re.compile(r'el(.+)?\d\d\d\d')
    self.ptrn_text = re.compile(r'<p>(.+?)<br class="clear">', re.DOTALL)
    self.ptrn_img1 = re.compile(r'class="wall_img_container_big"><img (.+)?jpg">')
    self.ptrn_img2 = re.compile(r'src="(.+)?')
    self.ptrn_img_e1 = re.compile(r'class="wall_img_container_big"><img (.+)?', re.DOTALL)
    self.ptrn_img_e2 = re.compile(r'http://(.+)?.jpg')
    self.ptrn_prev = re.compile(r'<a class="arrow_change_photo arrow_change_photo_right" href="(.+?)">›')
    self.ptrn_ffs = re.compile(r'<a class="userLink (.+?)" href="(.+?)">(.+?)</a>')
    self.ptrn_ffs_next = re.compile(r'<a href="(.+?)" class="next">')

  def get_web(self, web):
    w = urllib.urlopen(web)
    self.webcode = w.read()
    w.close()
  
  def init_carpeta(self):
    self.get_web(self.web)
    wdir = self.name
    mkdir_p(wdir)
    logging.info('Folder '+wdir+' created. Moving into it.')
    os.chdir('./' + wdir)
    print 'Creado directorio ' + wdir + '\n'
    logging.info('Os pointer inside folder '+wdir)
    
  def save_day(self):
    title = self.ptrn_title.search(self.webcode)
    if title == None:
      logging.warning('No title!')
      title = 'Sin título'
    else:
      title = title.group(1)
    
    day = self.ptrn_date.search(self.webcode)
    if day == None:
      day = 'Sin fecha'
    else:
      day = day.group(1)
      logging.info('Date matching result: ' + day)
      date = self.ptrn_date_format.search(day)
      if date == None:
        logging.error('Date not good format')
      else:
        day = date.group(1)
        logging.info('Date correctly obtained' + day)
    text = self.ptrn_text.search(self.webcode)
    if text == None:
      logging.warning('No text in post '+title +' from date '+day)
    else:
      text = text.group(1)

    img1 = self.ptrn_img1.search(self.webcode)
    img2 = None
    if img1 == None:
      logging.warning('No image found in post '+title +' from date '+day)
      logging.info('Trying different approach')
      img1 = self.ptrn_img_e1.search(self.webcode)
      if img1 == None:
        logging.error('Could not solve image issues. Reached step 1.')
      else:
        img1 = img1.group(1)
        img2 = self.ptrn_img_e2.search(img1)
        if img2  == None:
          logging.error('Could not solve image issues. Reached step 2.')
        else:
          img2 = img2.group(1)
          img2 = 'http://' + img2 + '.jpg'
          print img2
    else:
      img1 = img1.group(1)
      img2 = self.ptrn_img2.search(img1)
      if img2 == None:
        logging.warning('Image error in post '+title +' from date '+day)
      else:
        img2 = img2.group(1)+'jpg'
    mkdir_p(day)
    path = './' + day + '/'
    path2 = path + day
    num = 0
    while os.path.isfile(path2+'.html'): ## Añadido por si hay dos entradas el mismo día con el mismo nombre.
      path2 = path2+'_'+str(num)
      num += 1
    f = file(path2 + '.html', 'w')
    f.write(self.html_head)
    f.write(text)
    f.write(self.html_footer)
    f.close()
    urllib.urlretrieve(img2, path2 + '.jpg')
    logging.info('Entry saved. Post: '+title +' from date '+day)
    print self.entrada
    self.entrada = self.entrada + 1
  
  def get_prev(self):
    self.web = self.ptrn_prev.search(self.webcode)
    if self.web == None:
      return self.web
    else:
      self.web = self.web.group(1)
      self.get_web(self.web)
      return self.web

  def dump_images(self):
    img1 = self.ptrn_img1.search(self.webcode)
    img2 = None
    if img1 == None:
      logging.warning('No image found in post '+str(self.entrada))
      logging.info('Trying different approach')
      img1 = self.ptrn_img_e1.search(self.webcode)
      if img1 == None:
        logging.error('Could not solve image issues. Reached step 1.')
      else:
        img1 = img1.group(1)
        img2 = self.ptrn_img_e2.search(img1)
        if img2  == None:
          logging.error('Could not solve image issues. Reached step 2.')
        else:
          img2 = img2.group(1)
          img2 = 'http://' + img2 + '.jpg'
    else:
      img1 = img1.group(1)
      img2 = self.ptrn_img2.search(img1)
      if img2 == None:
        logging.warning('Image error in post '+str(self.entrada))
      else:
        img2 = img2.group(1)+'jpg'
    path = './'
    path2 = path + str(self.entrada)
    fpd = FotologPictureDownloader(img2,path2)
    fpd.run()
    #urllib.urlretrieve(img2, path2 + '.jpg')
    #logging.info('Entry saved. Post: '+str(self.entrada))
    print self.entrada
    self.entrada = self.entrada + 1 
      
  '''def save_friends(self):##modulo deshabilitado para evitar problemas
    web = self.web + '/ff'
    ffs_names = ''
    pagina = 1
    while web:
      self.get_web(web)
      ffs = self.ptrn_ffs.findall(self.webcode)
      #for i in ffs
      #poner not en vez de <> y no utilizar (.+?) si no se quiere guardar el resultado, con .+? ya tira.
      i = iter(ffs)
      item = i.next()
      ffs_names += item[2] + '\n'
      try:
        while item:
          item = i.next()
          ffs_names += item[2] + '\n'
      except StopIteration:
        print 'No hay más amigos en la página ' + str(pagina)
      web = self.ptrn_ffs_next.search(self.webcode)
      pagina += 1
      if web <> None:
        web = web.group(1)
    f = file('./ffs.txt', 'w')
    f.write('u'+ffs_names)
    f.close()'''
    def recursive_solve():
      if self.get_prev() != None:
        self.recursive_solve()
      else:
        print "Finished"
        return ""
      self.dump_images()



def main(argv):
  logging.basicConfig(filename='fotolog.crawl.log', level=logging.DEBUG)
  nom = raw_input("¿Que fotolog quieres descargar?\n")
  objectiu = fotolog(nom)
  objectiu.init_patrones()
 # objectiu.save_friends()
  objectiu.init_carpeta()
  if argv[0] != None:
    if argv[0] == '-di': #Dump Images
      objectiu.dump_images()
      while objectiu.get_prev():
        objectiu.dump_images()
  else:
    print 'Sin parámetros se ejecuta en modo normal.'
    objectiu.save_day()
    while objectiu.get_prev():
      objectiu.save_day()
  print '\nTodo ha sido descargado correctamente'

if __name__ == '__main__':
  main(sys.argv[1:])
